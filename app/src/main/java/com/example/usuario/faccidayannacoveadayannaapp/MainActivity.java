package com.example.usuario.faccidayannacoveadayannaapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    private EditText cent, far;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.cent = (EditText) findViewById(R.id.txtCentigrados);
        this.far = (EditText) findViewById(R.id.txtFarengeit);
        this.cent.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                //calculo

                float faren;
                faren = (1.8f) * Float.parseFloat(cent.getText().toString()) + 32;

                far.setText("" + faren);

                return false;
            }
        });
        this.far.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                //calculo
                float centi;
                centi = (Float.parseFloat(far.getText().toString()) - 32) / (1.8f);
                cent.setText("" + centi);
                return false;


            }


        });
    }
}